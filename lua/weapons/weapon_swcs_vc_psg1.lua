SWEP.Base = "weapon_swcs_base"
SWEP.Category = "SWCS Vice City"

SWEP.PrintName = "PSG1"
SWEP.Spawnable = true
SWEP.HoldType = "ar2"
SWEP.WorldModel = Model"models/weapons/csgo/w_psg1_dropped.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_psg1.mdl"

sound.Add({
	name = "Weapon_PSG1_CSGO.Single",
	channel = CHAN_STATIC,
	volume = 1.0,
	pitch = 100,
	level = 79,
	sound = Sound")weapons/csgo/PSG1/PSG-1_fp.wav"
})
sound.Add({
	name = "Weapon_PSG1_CSGO.BoltBack",
	channel = CHAN_ITEM,
	volume = 1.0,
	pitch = 100,
	level = 60,
	sound = Sound")weapons/csgo/PSG1/PSG-1_Boltback.wav"
})
sound.Add({
	name = "Weapon_PSG1_CSGO.BoltForward",
	channel = CHAN_ITEM,
	volume = 1.0,
	pitch = 100,
	level = 60,
	sound = Sound")weapons/csgo/PSG1/PSG-1_Boltrelease.wav"
})
sound.Add({
	name = "Weapon_PSG1_CSGO.Magout",
	channel = CHAN_ITEM,
	volume = {0.5, 1.0},
	pitch = {97, 105},
	level = 65,
	sound = Sound")weapons/csgo/PSG1/PSG-1_Magout.wav"
})
sound.Add({
	name = "Weapon_PSG1_CSGO.Magin",
	channel = CHAN_ITEM,
	volume = {0.5, 1.0},
	pitch = {97, 105},
	level = 65,
	sound = Sound")weapons/csgo/PSG1/PSG-1_Magin.wav"
})
sound.Add({
	name = "Weapon_PSG1_CSGO.Draw",
	channel = CHAN_STATIC,
	volume = 0.3,
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/PSG1/PSG-1_draw.wav"
})

SWEP.ItemDefAttributes = [=["attributes"
{
    "aimsight lens mask"		"models/weapons/shells/762psg.mdl"
    "magazine model"		"models/weapons/csgo/w_psg1_mag.mdl"
    "primary reserve ammo max"		"30"
    "inaccuracy jump initial"		"125"
    "inaccuracy jump"		"160"
    "inaccuracy jump alt"		"160"
    "heat per shot"		"0.35"
    "addon scale"		"0.9"
    "max player speed"		"225"
    "max player speed alt"		"166"
    "in game price"		"4800"
    "is full auto"		"0"
    "armor ratio"		"1.7"
    "crosshair min distance"		"6"
    "crosshair delta distance"		"4"
    "zoom levels"		"2"
    "zoom time 0"		"0.050000"
    "zoom fov 1"		"45"
    "zoom time 1"		"0.050000"
    "zoom fov 2"		"20"
    "zoom time 2"		"0.050000"
    "hide view model zoomed"		"1"
    "penetration"		"2.5"
    "damage"		"68"
    "range"		"8192"
    "cycletime"		"0.25"
    "time to idle"		"1.8"
    "idle interval"		"60"
    "flinch velocity modifier large"		"0.5"
    "flinch velocity modifier small"		"0.65"
    "spread"		"30"
    "inaccuracy crouch"		"3"
    "inaccuracy stand"		"6"
    "inaccuracy land"		"0.262000"
    "inaccuracy ladder"		"116.389999"
    "inaccuracy fire"		"44"
    "inaccuracy move"		"50"
    "spread alt"		"0.3"
    "inaccuracy crouch alt"		"1.45"
    "inaccuracy stand alt"		"1.7"
    "inaccuracy land alt"		"0.262000"
    "inaccuracy ladder alt"		"116.389999"
    "inaccuracy fire alt"		"3"
    "inaccuracy move alt"		"100"
    "recovery time crouch"		"0.388808"
    "recovery time stand"		"0.544331"
    "recoil angle"		"0"
    "recoil angle variance"		"66"
    "recoil magnitude"		"61"
    "recoil magnitude variance"		"19"
    "recoil seed"		"29908"
    "primary clip size"		"10"
    "weapon weight"		"20"
    "rumble effect"		"4"
    "recoil angle alt"		"0"
    "recoil angle variance alt"		"30"
    "recoil magnitude alt"		"30"
    "recoil magnitude variance alt"		"4"
    "recovery time crouch final"		"0.388808"
    "recovery time stand final"		"0.544331"
}]=]
SWEP.ItemDefVisuals = [=["visuals"
{
    "muzzle_flash_effect_1st_person"		"weapon_muzzle_flash_huntingrifle"
    "muzzle_flash_effect_3rd_person"		"weapon_muzzle_flash_huntingrifle"
    "heat_effect"		"weapon_muzzle_smoke"
    "addon_location"		"primary_sniper"
    "eject_brass_effect"		"shell_762psg"
    "tracer_effect"		"weapon_tracers_rifle"
    "weapon_type"		"SniperRifle"
    "player_animation_extension"		"g3"
    "primary_ammo"		"BULLET_PLAYER_762MM"
    "sound_single_shot"		"Weapon_PSG1_CSGO.Single"
    "sound_nearlyempty"		"Default.nearlyempty"
}]=]
