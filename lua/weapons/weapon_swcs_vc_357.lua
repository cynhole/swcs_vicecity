SWEP.Base = "weapon_swcs_base"
SWEP.Category = "SWCS Vice City"

SWEP.PrintName = "Colt Python"
SWEP.Spawnable = true
SWEP.HoldType = "ar2"
SWEP.WorldModel = Model"models/weapons/csgo/w_357_dropped.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_357.mdl"

sound.Add({
	name = "AK_MLG_357.Single",
	channel = CHAN_STATIC,
	volume = 0.9,
	pitch = 100,
	level = 79,
	sound = Sound")weapons/csgo/357/357-1.wav"
})
sound.Add({
	name = "AK_MLG_357.Draw",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/draw.wav"
})
sound.Add({
	name = "AK_MLG_357.In",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/in.wav"
})
sound.Add({
	name = "AK_MLG_357.Out",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/out.wav"
})
sound.Add({
	name = "AK_MLG_357.Spin",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/spin.wav"
})
sound.Add({
	name = "AK_MLG_357.Close",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/close.wav"
})
sound.Add({
	name = "AK_MLG_357.Open",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/open.wav"
})
sound.Add({
	name = "AK_MLG_357.Hammer",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/hammer.wav"
})
sound.Add({
	name = "AK_MLG_357.Cloth",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/cloth.wav"
})
sound.Add({
	name = "AK_MLG_357.Cloth2",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/cloth2.wav"
})
sound.Add({
	name = "AK_MLG_357.Cloth3",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/cloth3.wav"
})
sound.Add({
	name = "AK_MLG_357.InSpin",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/357/inspin.wav"
})

SWEP.ItemDefAttributes = [=["attributes"
{
    "heat per shot"		"0.300000"
    "addon scale"		"1.000000"
    "tracer frequency"		"1"
    "primary clip size"		"6"
    "primary default clip size"		"-1"
    "secondary default clip size"		"-1"
    "is full auto"		"0"
    "max player speed"		"230"
    "in game price"		"850"
    "armor ratio"		"1.7"
    "crosshair min distance"		"8"
    "crosshair delta distance"		"3"
    "cycletime"		"0.225000"
    "model right handed"		"1"
    "penetration"		"2"
    "damage"		"60"
    "range"		"8192"
    "range modifier"		"0.810000"
    "bullets"		"1"
    "cycletime"		"0.225000"
    "flinch velocity modifier large"		"0.400000"
    "flinch velocity modifier small"		"0.550000"
    "spread"		"1.000000"
    "inaccuracy crouch"		"2"
    "inaccuracy stand"		"4"
    "inaccuracy jump initial"		"215"
    "inaccuracy jump"		"370"
    "inaccuracy land"		"0.7"
    "inaccuracy ladder"		"150"
    "inaccuracy fire"		"71"
    "inaccuracy move"		"47"
    "recovery time crouch"		"0.35"
    "recovery time stand"		"0.65"
    "recoil angle"		"0.000000"
    "recoil angle variance"		"60"
    "recoil magnitude"		"48.200001"
    "recoil magnitude variance"		"18"
    "recoil seed"		"1454"
    "primary reserve ammo max"		"60"
    "weapon weight"		"7"
    "rumble effect"		"2"
    "inaccuracy crouch alt"		"2.180000"
    "inaccuracy fire alt"		"72.230003"
    "inaccuracy jump alt"		"371.549988"
    "inaccuracy ladder alt"		"152.000000"
    "inaccuracy land alt"		"0.730000"
    "inaccuracy move alt"		"48.099998"
    "inaccuracy stand alt"		"4.200000"
    "max player speed alt"		"230"
    "recoil angle variance alt"		"60"
    "recoil magnitude alt"		"48.200001"
    "recoil magnitude variance alt"		"18"
    "recovery time crouch final"		"0.449927"
    "recovery time stand final"		"0.811200"
    "spread alt"		"2.000000"
}]=]
SWEP.ItemDefVisuals = [=["visuals"
{
    "muzzle_flash_effect_1st_person"		"weapon_muzzle_flash_pistol"
    "muzzle_flash_effect_3rd_person"		"weapon_muzzle_flash_pistol"
    "heat_effect"		"weapon_muzzle_smoke"
    "eject_brass_effect"		"weapon_shell_casing_9mm"
    "tracer_effect"		"weapon_tracers_mach"
    "weapon_type"		"Pistol"
    "player_animation_extension"		"pistol"
    "primary_ammo"		"BULLET_PLAYER_50AE"
    "sound_single_shot"		"AK_MLG_357.Single"
    "sound_nearlyempty"		"Default.nearlyempty"
}]=]
