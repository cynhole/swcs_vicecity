SWEP.Base = "weapon_swcs_base"
SWEP.Category = "SWCS Vice City"

SWEP.PrintName = "M4"
SWEP.Spawnable = true
SWEP.HoldType = "ar2"
SWEP.WorldModel = Model"models/weapons/csgo/w_m4_dropped.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_m4.mdl"

sound.Add({
	name = "Weapon_m4_CSGO.Single",
	channel = CHAN_STATIC,
	volume = 0.65,
	level = 94,
	pitch = {100, 105},
	sound = Sound")weapons/csgo/ar15/ar15_fp.wav"
})
sound.Add({
	name = "Weapon_m4_CSGO.Magin",
	channel = CHAN_ITEM,
	volume = 0.2,
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/ar15/ar15_magin.wav"
})
sound.Add({
	name = "Weapon_m4_CSGO.Magout",
	channel = CHAN_ITEM,
	volume = 0.2,
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/ar15/ar15_magout.wav"
})
sound.Add({
	name = "Weapon_m4_CSGO.Boltrelease",
	channel = CHAN_ITEM,
	volume = 0.2,
	pitch = 100,
	level = 65,
	sound = Sound")weapons/csgo/ar15/ar15_boltrelease.wav"
})

SWEP.ItemDefAttributes = [=["attributes"
{
    "aimsight lens mask"		"models/weapons/shells/556.mdl"
    "magazine model"		"models/weapons/csgo/w_m4_mag.mdl"
    "primary reserve ammo max"		"90"
    "recovery time crouch"		"0.26"
    "recovery time crouch final"		"0.334"
    "recovery time stand"		"0.3"
    "recovery time stand final"		"0.45"
    "inaccuracy jump initial"		"100"
    "inaccuracy jump alt"		"120"
    "heat per shot"		"0.350000"
    "addon scale"		"1.000000"
    "tracer frequency"		"1"
    "max player speed"		"225"
    "is full auto"		"1"
    "in game price"		"2800"
    "armor ratio"		"1.7"
    "penetration"		"2"
    "damage"		"30"
    "range"		"8192"
    "range modifier"		"0.95"
    "cycletime"		"0.08"
    "time to idle"		"1.5"
    "idle interval"		"60"
    "flinch velocity modifier large"		"0.40"
    "flinch velocity modifier small"		"0.550"
    "spread"		"2"
    "inaccuracy crouch"		"3"
    "inaccuracy stand"		"6"
    "inaccuracy jump"		"120"
    "inaccuracy land"		"0.192000"
    "inaccuracy ladder"		"110"
    "inaccuracy fire"		"6"
    "inaccuracy move"		"60"
    "spread alt"		"0.450000"
    "inaccuracy crouch alt"		"3.680000"
    "inaccuracy stand alt"		"4.900000"
    "inaccuracy land alt"		"0.197000"
    "inaccuracy ladder alt"		"113.671997"
    "inaccuracy fire alt"		"6.340000"
    "inaccuracy move alt"		"122.000000"
    "recoil seed"		"38965"
    "recoil angle"		"7"
    "recoil angle variance"		"55"
    "recoil magnitude"		"21"
    "recoil magnitude variance"		"6"
    "primary clip size"		"30"
    "weapon weight"		"25"
    "rumble effect"		"4"
    "max player speed alt"		"225"
    "recoil angle alt"		"0"
    "recoil angle variance alt"		"70"
    "recoil magnitude alt"		"23"
    "recoil magnitude variance alt"		"0"
}]=]
SWEP.ItemDefVisuals = [=["visuals"
{
    "muzzle_flash_effect_1st_person"		"weapon_muzzle_flash_assaultrifle"
    "muzzle_flash_effect_3rd_person"		"weapon_muzzle_flash_assaultrifle"
    "heat_effect"		"weapon_muzzle_smoke"
    "addon_location"		"primary_rifle"
    "eject_brass_effect"		"shell_556_rifle"
    "tracer_effect"		"weapon_tracers_mach"
    "weapon_type"		"Rifle"
    "player_animation_extension"		"m4"
    "primary_ammo"		"BULLET_PLAYER_556MM"
    "sound_single_shot"		"Weapon_M4_CSGO.Single"
    "sound_special1"		"Weapon_M4A1_CSGO.Silenced"
    "sound_nearlyempty"		"Default.nearlyempty"
}]=]
