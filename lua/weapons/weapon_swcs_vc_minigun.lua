SWEP.Base = "weapon_swcs_base"
SWEP.Category = "SWCS Vice City"

-- this is the only weapon i do this for
SWEP.Primary.Ammo = "none"

SWEP.PrintName = "Minigun"
SWEP.Spawnable = true
SWEP.HoldType = "ar2"
SWEP.WorldModel = Model"models/weapons/csgo/w_minigun_dropped.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_minigun.mdl"

sound.Add({
	name = "Weapon_Minigun_CSGO.Single",
	channel = CHAN_STATIC,
	level = 79,
	volume = 0.3,
	pitch = {90, 95},
	sound = Sound")weapons/csgo/minigun/fire.wav"
})
sound.Add({
	name = "Weapon_Minigun_CSGO.Draw",
	channel = CHAN_ITEM,
	level = 79,
	volume = 0.2,
	sound = Sound")weapons/csgo/minigun/draw.wav"
})
sound.Add({
	name = "Weapon_Minigun_CSGO.EndSpin",
	channel = CHAN_ITEM,
	level = 79,
	volume = {0.77, 08},
	sound = Sound")weapons/csgo/minigun/minigun_wind_down.wav"
})

sound.Add({
	name = "Weapon_Minigun_CSGO.Movement1",
	channel = CHAN_ITEM,
	level = 79,
	volume = {0.77, 0.8},
	sound = Sound")weapons/csgo/minigun/movement1.wav"
})
sound.Add({
	name = "Weapon_Minigun_CSGO.Movement2",
	channel = CHAN_ITEM,
	level = 79,
	volume = {0.77, 0.8},
	sound = Sound")weapons/csgo/minigun/movement2.wav"
})
sound.Add({
	name = "Weapon_Minigun_CSGO.Movement3",
	channel = CHAN_ITEM,
	level = 79,
	volume = {0.77, 0.8},
	sound = Sound")weapons/csgo/minigun/movement3.wav"
})
sound.Add({
	name = "Weapon_Minigun_CSGO.Movement4",
	channel = CHAN_ITEM,
	level = 79,
	volume = {0.77, 0.8},
	sound = Sound")weapons/csgo/minigun/movement4.wav"
})
sound.Add({
	name = "Weapon_Minigun_CSGO.Movement5",
	channel = CHAN_ITEM,
	level = 79,
	volume = {0.77, 0.8},
	sound = Sound")weapons/csgo/minigun/movement5.wav"
})

SWEP.ItemDefAttributes = [=["attributes"
{
    "aimsight lens mask"		"models/weapons/csgo/shells/762x51.mdl"
    "primary reserve ammo max"		"-1"
    "inaccuracy jump initial"		"100"
    "inaccuracy jump"		"100"
    "heat per shot"		"0.35"
    "addon scale"		"0.9"
    "tracer frequency"		"1"
    "max player speed"		"150"
    "attack movespeed factor"  "0.55"
    "is full auto"		"1"
    "in game price"		"8500"
    "armor ratio"		"1.8"
    "crosshair min distance"		"6"
    "penetration"		"2"
    "damage"		"30"
    "range"		"8192"
    "range modifier"		"0.95"
    "cycletime"		"0.03"
    "time to idle"		"1.6"
    "flinch velocity modifier large"		"0.1"
    "flinch velocity modifier small"		"0.05"
    "spread"		"25"
    "inaccuracy crouch"		"2.5"
    "inaccuracy stand"		"5"
    "inaccuracy land"		"0.4000"
    "inaccuracy ladder"		"200"
    "inaccuracy fire"		"2.5"
    "inaccuracy move"		"10"
    "recovery time crouch"		"0.25"
    "recovery time stand"		"0.5"
    "recoil angle"		"1"
    "recoil angle variance"		"0"
    "recoil magnitude"		"5"
    "recoil magnitude variance"		"2"
    "recoil seed"		"0"
    "primary clip size"		"100"
    "weapon weight"		"100"
    "rumble effect"		"2"
    "inaccuracy crouch alt"		"5.340000"
    "inaccuracy fire alt"		"8.560000"
    "inaccuracy jump alt"		"15.470001"
    "inaccuracy ladder alt"		"200.809998"
    "inaccuracy land alt"		"0.4"
    "inaccuracy move alt"		"20.250000"
    "inaccuracy stand alt"		"8.700000"
    "max player speed alt"		"150"
    "recoil angle alt"		"1"
    "recoil angle variance alt"		"0"
    "recoil magnitude alt"		"0"
    "recoil magnitude variance alt"		"0"
    "recovery time crouch final"		"0"
    "recovery time stand final"		"0"
    "spread alt"		"2"
}]=]
SWEP.ItemDefVisuals = [=["visuals"
{
    "muzzle_flash_effect_1st_person"		"weapon_muzzle_flash_assaultrifle"
    "muzzle_flash_effect_3rd_person"		"weapon_muzzle_flash_assaultrifle"
    "heat_effect"		"weapon_muzzle_smoke"
    "addon_location"		"primary_mg"
    "eject_brass_effect"		"shell_762x51"
    "tracer_effect"		"weapon_tracers_mach"
    "weapon_type"		"Machinegun"
    "player_animation_extension"		"m249"
    "primary_ammo"		"BULLET_PLAYER_556MM_BOX"
    "sound_single_shot"		"Weapon_Minigun_CSGO.Single"
    "sound_nearlyempty"		"Default.nearlyempty"
}]=]
