SWEP.Base = "weapon_swcs_nova"
SWEP.Category = "SWCS Vice City"

SWEP.PrintName = "M37"
SWEP.Spawnable = true
SWEP.HoldType = "ar2"
SWEP.WorldModel = Model"models/weapons/csgo/w_m37_dropped.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_m37.mdl"

sound.Add({
	name = "Weapon_M37_CSGO.Single",
	channel = CHAN_STATIC,
	level = 94,
	volume = 1.0,
	pitch = {100, 105},
	sound = Sound")weapons/csgo/M37/M37-1.wav"
})
sound.Add({
	name = "Weapon_M37_CSGO.Insert",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.2,
	pitch = 100,
	sound = {Sound")weapons/csgo/M37/M37_insert1.wav",Sound")weapons/csgo/M37/M37_insert2.wav",Sound")weapons/csgo/M37/M37_insert3.wav"}
})
sound.Add({
	name = "Weapon_M37_CSGO.Pump",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.2,
	pitch = 100,
	sound = Sound"weapons/csgo/M37/M37_pump.wav"
})

sound.Add({
	name = "Universal.Draw",
	channel = CHAN_ITEM,
	level = 75,
	volume = 0.35,
	pitch = 96,
	sound = {Sound"weapons/universal/uni_weapon_draw_01.wav",Sound"weapons/universal/uni_weapon_draw_02.wav",Sound"weapons/universal/uni_weapon_draw_03.wav"}
})
sound.Add({
	name = "Universal.LeanIn",
	channel = CHAN_STATIC,
	level = 75,
	volume = 0.15,
	pitch = {95, 105},
	sound = {
		Sound"weapons/universal/uni_lean_in_01.wav",Sound"weapons/universal/uni_lean_in_02.wav",
		Sound"weapons/universal/uni_lean_in_03.wav",Sound"weapons/universal/uni_lean_in_04.wav",
		Sound"weapons/universal/uni_lean_out_01.wav",Sound"weapons/universal/uni_lean_out_01.wav",
		Sound"weapons/universal/uni_lean_out_03.wav",Sound"weapons/universal/uni_lean_out_04.wav",
	}
})

SWEP.ItemDefAttributes = [=["attributes"
{
    "aimsight lens mask"		"models/weapons/shells/shell_12g.mdl"
    "primary reserve ammo max"		"35"
    "is full auto"		"0"
    "inaccuracy jump initial"		"50"
    "inaccuracy jump"		"60"
    "heat per shot"		"1.50"
    "addon scale"		"0.90"
    "tracer frequency"		"1"
    "max player speed"		"230"
    "in game price"		"1200"
    "kill award"		"900"
    "armor ratio"		"1.5"
    "crosshair min distance"		"8"
    "crosshair delta distance"		"6"
    "penetration"		"1"
    "damage"		"18"
    "range"		"3000"
    "range modifier"		"0.8"
    "bullets"		"9"
    "cycletime"		"0.66666666667"
    "flinch velocity modifier large"		"0.400000"
    "flinch velocity modifier small"		"0.450000"
    "spread"		"20"
    "inaccuracy crouch"		"10"
    "inaccuracy stand"		"15"
    "inaccuracy land"		"0.236000"
    "inaccuracy ladder"		"78.750000"
    "inaccuracy fire"		"150"
    "inaccuracy move"		"20"
    "recovery time crouch"		"0.328941"
    "recovery time stand"		"0.460517"
    "recoil angle"		"6"
    "recoil angle variance"		"20"
    "recoil magnitude"		"85"
    "recoil magnitude variance"		"25"
    "recoil seed"		"7763"
    "spread seed"		"17514"
    "primary clip size"		"7"
    "weapon weight"		"20"
    "rumble effect"		"5"
    "inaccuracy crouch alt"		"5.250000"
    "inaccuracy fire alt"		"9.720000"
    "inaccuracy jump alt"		"126.309998"
    "inaccuracy ladder alt"		"78.750000"
    "inaccuracy land alt"		"0.236000"
    "inaccuracy move alt"		"36.750000"
    "inaccuracy stand alt"		"7.000000"
    "max player speed alt"		"220"
    "recoil angle alt"		"0"
    "recoil angle variance alt"		"20"
    "recoil magnitude alt"		"143"
    "recoil magnitude variance alt"		"22"
    "recovery time crouch final"		"0.328941"
    "recovery time stand final"		"0.460517"
    "spread alt"		"40.000000"
}]=]
SWEP.ItemDefVisuals = [=["visuals"
{
    "muzzle_flash_effect_1st_person"		"weapon_muzzle_flash_autoshotgun"
    "muzzle_flash_effect_3rd_person"		"weapon_muzzle_flash_autoshotgun"
    "heat_effect"		"weapon_muzzle_smoke"
    "addon_location"		"primary_shotgun"
    "eject_brass_effect"		"shell_12g"
    "tracer_effect"		"weapon_tracers_assrifle"
    "weapon_type"		"Shotgun"
    "player_animation_extension"		"m37"
    "primary_ammo"		"BULLET_PLAYER_BUCKSHOT"
    "sound_single_shot"		"Weapon_M37_CSGO.Single"
    "sound_nearlyempty"		"Default.nearlyempty"
}]=]
