SWEP.Base = "weapon_swcs_base"
SWEP.Category = "SWCS Vice City"

SWEP.PrintName = "Uzi"
SWEP.Spawnable = true
SWEP.HoldType = "ar2"
SWEP.WorldModel = Model"models/weapons/csgo/w_uzi_dropped.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_uzi.mdl"

sound.Add({
	name = "Weapon_UZI_CSGO.Single",
	channel = CHAN_STATIC,
	volume = 0.65,
	pitch = 100,
	level = 94,
	sound = Sound")weapons/csgo/uzi/uzi_fp.wav"
})
sound.Add({
	name = "Weapon_UZI_CSGO.Draw",
	channel = CHAN_ITEM,
	volume = 0.4,
	pitch = 100,
	sound = Sound")weapons/csgo/uzi/draw.wav"
})
sound.Add({
	name = "Weapon_UZI_CSGO.Clipout",
	channel = CHAN_ITEM,
	volume = 0.4,
	pitch = 100,
	sound = Sound")weapons/csgo/uzi/clipout.wav"
})
sound.Add({
	name = "Weapon_UZI_CSGO.Cliptap",
	channel = CHAN_ITEM,
	volume = 0.4,
	pitch = 100,
	sound = Sound")weapons/csgo/uzi/cliptap.wav"
})
sound.Add({
	name = "Weapon_UZI_CSGO.Clipin",
	channel = CHAN_ITEM,
	volume = 0.4,
	pitch = 100,
	sound = Sound")weapons/csgo/uzi/clipin.wav"
})
sound.Add({
	name = "Weapon_UZI_CSGO.Boltpull",
	channel = CHAN_ITEM,
	volume = 0.4,
	pitch = 100,
	sound = Sound")weapons/csgo/uzi/boltpull.wav"
})
sound.Add({
	name = "Weapon_UZI_CSGO.Boltrelease",
	channel = CHAN_ITEM,
	volume = 0.4,
	pitch = 100,
	sound = Sound")weapons/csgo/uzi/boltrelease.wav"
})

SWEP.ItemDefAttributes = [=["attributes"
{
    "aimsight lens mask"		"models/weapons/shells/9mm.mdl"
    "magazine model"		"models/weapons/w_uzi_mag.mdl"
    "primary reserve ammo max"		"128"
    "inaccuracy jump initial"		"75"
    "inaccuracy jump"		"80"
    "heat per shot"		"0.350000"
    "tracer frequency"		"1"
    "max player speed"		"220"
    "is full auto"		"1"
    "in game price"		"1200"
    "kill award"		"600"
    "armor ratio"		"1.6"
    "crosshair min distance"		"6"
    "crosshair delta distance"		"2"
    "penetration"		"1"
    "damage"		"29"
    "range"		"3600"
    "range modifier"		"0.9"
    "cycletime"		"0.1"
    "time to idle"		"2"
    "flinch velocity modifier large"		"0.000000"
    "flinch velocity modifier small"		"0.000000"
    "spread"		"2"
    "inaccuracy crouch"		"1"
    "inaccuracy stand"		"3"
    "inaccuracy land"		"0.115000"
    "inaccuracy ladder"		"75"
    "inaccuracy fire"		"4"
    "inaccuracy move"		"23"
    "recovery time crouch"		"0.3"
    "recovery time stand"		"0.36"
    "recoil angle"		"2"
    "recoil angle variance"		"56"
    "recoil magnitude"		"15"
    "recoil magnitude variance"		"1"
    "recoil seed"		"61649"
    "primary clip size"		"32"
    "weapon weight"		"25"
    "rumble effect"		"3"
    "inaccuracy crouch alt"		"5.920000"
    "inaccuracy fire alt"		"2.180000"
    "inaccuracy jump alt"		"59.599998"
    "inaccuracy ladder alt"		"57.560001"
    "inaccuracy land alt"		"0.115000"
    "inaccuracy move alt"		"19.860001"
    "inaccuracy stand alt"		"10.000000"
    "max player speed alt"		"220"
    "recoil angle alt"		"0"
    "recoil angle variance alt"		"70"
    "recoil magnitude alt"		"16"
    "recoil magnitude variance alt"		"1"
    "recovery time crouch final"		"0.312494"
    "recovery time stand final"		"0.437491"
    "spread alt"		"0.600000"
}]=]
SWEP.ItemDefVisuals = [=["visuals"
{
    "muzzle_flash_effect_1st_person"		"weapon_muzzle_flash_smg"
    "muzzle_flash_effect_3rd_person"		"weapon_muzzle_flash_smg"
    "heat_effect"		"weapon_muzzle_smoke"
    "addon_location"		"primary_smg"
    "eject_brass_effect"		"shell_9mm"
    "tracer_effect"		"weapon_tracers_smg"
    "weapon_type"		"SubMachinegun"
    "player_animation_extension"		"mp7"
    "primary_ammo"		"BULLET_PLAYER_9MM"
    "secondary_ammo"		"BULLET_PLAYER_9MM"
    "sound_single_shot"		"Weapon_UZI_CSGO.Single"
    "sound_nearlyempty"		"Default.nearlyempty"
}]=]
